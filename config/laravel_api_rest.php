<?php

return array(
    /*
        |--------------------------------------------------------------------------
        | Host Server
        |--------------------------------------------------------------------------
    */
    'host' => 'myapi.dev/',

    /*
        |--------------------------------------------------------------------------
        | Relative path to access parsed swagger annotations.
        |--------------------------------------------------------------------------
    */
    'doc-route' => 'docs',

    /*
       |--------------------------------------------------------------------------
       | Relative path to access swagger ui.
       |--------------------------------------------------------------------------
    */
    'api-docs-route' => 'api/docs',

    /*
       |--------------------------------------------------------------------------
       | Edit to set the api's version number
       |--------------------------------------------------------------------------
    */
    "default-api-version" => "v1",

    /*
       |--------------------------------------------------------------------------
       | Edit to set the swagger version number
       |--------------------------------------------------------------------------
    */
    "default-swagger-version" => "2.0",

    /*
       |--------------------------------------------------------------------------
       | Edit to set the api's base path
       |--------------------------------------------------------------------------
    */
    "default-base-path" => "api/v1",

    /*
       |--------------------------------------------------------------------------
       | Edit to set the api's base path
       |--------------------------------------------------------------------------
    */
    "middleware" => ['jwt.auth'],

    /*
       |--------------------------------------------------------------------------
       | Resources
       |--------------------------------------------------------------------------
    */
    "resources" => [
        'abonnement' => [
            'model' => 'App\Model\Abonnement',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'distributeur' => [
            'model' => 'App\Model\Distributeur',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'employe' => [
            'model' => 'App\Model\Employe',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'film' => [
            'model' => 'App\Model\Film',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'fonction' => [
            'model' => 'App\Model\Fonction',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'forfait' => [
            'model' => 'App\Model\Forfait',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'genre' => [
            'model' => 'App\Model\Genre',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'membre' => [
            'model' => 'App\Model\Membre',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'personne' => [
            'model' => 'App\Model\Personne',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'reduction' => [
            'model' => 'App\Model\Reduction',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'salle' => [
            'model' => 'App\Model\Salle',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ],
        'seance' => [
            'model' => 'App\Model\Seance',
            'actions' => [
                'index',
                'show',
                'create',
                'update',
                'delete'
            ]
        ]
    ]
);