<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Models\Abonnement;
use App\Models\Forfait;
use App\Models\Membre;
use App\Models\Seance;
use App\Models\Personne;
use App\Models\Employe;
use App\Models\Fonction;
use App\Models\Distributeur;
use App\Models\Film;
use App\Models\Salle;
use App\Models\Reduction;
use App\Models\Genre;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Users
        DB::table('users')->delete();
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin',
            'password' => \Hash::make('admin'),
            'is_admin' => 1
        ]);
        foreach (range(1,10) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => \Hash::make('admin')
            ]);
        }

        $this->command->info('The users are created!');

        // Forfaits
        DB::table('forfaits')->delete();
        foreach (range(1,100) as $index) {
            Forfait::create(array(
                'nom' => $faker->name,
                'resum' => $faker->text,
                'prix' => $faker->numberBetween(0, 100),
                'duree_jours' => $faker->numberBetween(10, 95)
            ));
        }

        $this->command->info('The abonnements are created!');

        // Abonnements
        DB::table('abonnements')->delete();
        $forfaits = Forfait::all()->pluck('id_forfait');
        foreach (range(1,100) as $index) {
            Abonnement::create(array(
                'debut' => $faker->dateTimeBetween('-1 years'),
                'id_forfait' => $faker->randomElement($forfaits->toArray())
            ));
        }

        $this->command->info('The abonnements are created!');

        // Personnes
        DB::table('personnes')->delete();

        foreach (range(1,100) as $index) {
            Personne::create(array(
                'nom' => $faker->name,
                'prenom' => $faker->firstName,
                'date_naissance' => $faker->dateTimeBetween('-1 years'),
                'email' => $faker->email,
                'adresse' => $faker->address,
                'cpostal' => $faker->postcode,
                'ville' => $faker->city,
                'pays' => $faker->country
            ));
        }

        $this->command->info('The personnes are created!');

        // Fonctions
        DB::table('fonctions')->delete();
        foreach (range(1,100) as $index) {
            Fonction::create(array(
                'nom' => $faker->name,
                'salaire' => $faker->numberBetween(1600, 2000),
                'cadre' => $faker->boolean()
            ));
        }

        $this->command->info('The employes are created!');

        // Employees
        DB::table('employes')->delete();
        $personnes = Personne::all()->pluck('id_personne');
        $fonctions = Fonction::all()->pluck('id_fonction');
        foreach (range(1,100) as $index) {
            Employe::create(array(
                'id_personne' => $faker->randomElement($personnes->toArray()),
                'id_fonction' => $faker->randomElement($fonctions->toArray()),
            ));
        }

        $this->command->info('The employees are created!');

        // Membres
        DB::table('membres')->delete();
        $abonnements = Abonnement::all()->pluck('id_abonnement');
        foreach (range(1,100) as $index) {
            Membre::create(array(
                'id_personne' => $faker->randomElement($personnes->toArray()),
                'id_abonnement' => $faker->randomElement($abonnements->toArray()),
                'date_inscription' => $faker->dateTimeBetween('-1 years'),
                'debut_abonnement' => $faker->dateTimeBetween('-1 years')

            ));
        }

        $this->command->info('The membres are created!');

        // Genres
        DB::table('genres')->delete();
        foreach (range(1,100) as $index) {
            Genre::create(array(
                'nom' => $faker->name
            ));
        }

        $this->command->info('The genres are created!');

        // Films
        DB::table('films')->delete();
        $genres = Genre::all()->pluck('id_genre');
        $distributeurs = Distributeur::all()->pluck('id_distributeur');
        foreach (range(1,100) as $index) {
            Film::create(array(
                'id_genre' => $faker->randomElement($genres->toArray()),
                'id_distributeur' => $faker->randomElement($distributeurs->toArray()),
                'titre' => $faker->title,
                'resum' => $faker->text,
                'date_debut_affiche' => $faker->dateTimeBetween('-1 years'),
                'date_fin_affiche' => $faker->dateTimeBetween('-1 years'),
                'duree_minutes' => $faker->numberBetween(1, 60),
                'annee_production' => $faker->year

            ));
        }

        $this->command->info('The films are created!');

        // Salles
        DB::table('salles')->delete();
        foreach (range(1,100) as $index) {
            Salle::create(array(
                'numero_salle' => $faker->numberBetween(1, 12),
                'nom_salle' => $faker->name,
                'etage_salle' => $faker->numberBetween(1, 3),
                'places' => $faker->numberBetween(1, 200)

            ));
        }

        $this->command->info('The seances are created!');

        // Seances
        DB::table('seances')->delete();
        $films = Film::all()->pluck('id_film');
        $salles = Salle::all()->pluck('id_salle');
        foreach (range(1,100) as $index) {
            Seance::create(array(
                'id_film' => $faker->randomElement($films->toArray()),
                'id_salle' => $faker->randomElement($salles->toArray()),
                'id_personne_ouvreur' => $faker->randomElement($personnes->toArray()),
                'id_personne_technicien' => $faker->randomElement($personnes->toArray()),
                'id_personne_menage' => $faker->randomElement($personnes->toArray()),
                'debut_seance' => $faker->dateTimeBetween('-1 years'),
                'fin_seance' => $faker->dateTimeBetween('-1 years')

            ));
        }

        $this->command->info('The seances are created!');

        // Reductions
        DB::table('reductions')->delete();
        foreach (range(1,100) as $index) {
            Reduction::create(array(
                'nom' => $faker->name,
                'date_debut' => $faker->dateTimeBetween('-1 years'),
                'date_fin' => $faker->dateTimeBetween('-1 years'),
                'pourcentage_reduction' => $faker->numberBetween(1, 50)

            ));
        }

        $this->command->info('The reductions are created!');

    }
}