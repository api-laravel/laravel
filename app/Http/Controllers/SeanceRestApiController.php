<?php
namespace App\Http\Controllers;
use \Waties\LaravelApiRest\Http\Controllers\RestController as RestController;
use App\Models\Seance;
use \Illuminate\Http\Request;
class SeanceRestApiController extends RestController {

    public function index(Request $request)
    {
        $results = null;
        try {
            $statusCode = 206;

            /* Parameters */
            $perPage = $request->query->getInt('per_page', 25);
            $this->config->setRequest($request);
            $sorting = $this->config->getSorting();

            $resourceClassName = $this->config->getResourceClassName();
            $model = null;

            foreach ($sorting as $key => $order) {
                if (!isset($model)) {
                    $model = $resourceClassName::orderBy($key, $order);
                } else {
                    Seance::with('films')->where('date_fin_affiche','<', $request->endDate)->get();
                }
            }

            $results = $model->paginate($perPage)->load($this->relations);
        } catch (\Exception $e) {
            $statusCode = 400;
        } finally {
            return \Response::json($results, $statusCode);
        }
    }

}