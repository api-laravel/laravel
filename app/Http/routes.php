<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/api/v1/authenticate', [
    'as' => 'authenticate',
    'uses' => 'JwtController@authenticate'
]);

Route::post('hashpassword', [
    'as' => 'hashpassword',
    'uses' => 'JwtController@hashPassword',
]);

Route::get(Config::get('laravel_api_rest.default-base-path') . '/seance/{endDate}', 'SeanceRestApiController@index');