<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forfait extends Model {

    protected $primaryKey = 'id_forfait';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('nom', 'resum', 'prix', 'duree_jours');


}