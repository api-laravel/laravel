<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fonction extends Model {

    protected $primaryKey = 'id_fonction';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('nom', 'salaire', 'cadre');
}