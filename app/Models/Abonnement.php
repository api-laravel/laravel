<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model {

    protected $primaryKey = 'id_abonnement';

    public $timestamps = false;

    protected $relations = array('forfait');

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('debut');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function forfait() {
        return $this->belongsTo('App\Models\Forfait', 'id_forfait');
    }
    public function membres() {
        return $this->hasMany('App\Models\Membre', 'id_abonnement');
    }


}