<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personne extends Model {

    protected $primaryKey = 'id_personne';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('nom', 'prenom', 'date_naissance', 'email', 'adresse', 'cpostal', 'ville', 'pays');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function personne() {
        return $this->belongsTo('App\Models\Personne', 'id_personne');
    }
    public function membres() {
        return $this->hasMany('App\Models\Membre', 'id_personne');
    }

}