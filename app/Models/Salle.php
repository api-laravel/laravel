<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model {

    protected $primaryKey = 'id_salle';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('numero_salle', 'nom_salle', 'etage_salle', 'places');

    public function seances(){
        return $this->hasMany('App\Models\Seance','id_salle');
    }
}