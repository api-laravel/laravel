<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 14/04/2016
 * Time: 14:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class HistoriqueMembre extends Model
{

    protected $table = 'historique_membre';

    protected $primaryKey = 'id_historique';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('id_membre', 'id_seance', 'date');
}