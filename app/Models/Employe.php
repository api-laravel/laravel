<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model {

    protected $primaryKey = 'id_employe';

    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function personne() {
        return $this->belongsTo('App\Models\Personne', 'id_personne');
    }

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function fonction() {
        return $this->belongsTo('App\Models\Fonction', 'id_fonction');
    }
}