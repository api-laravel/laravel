<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membre extends Model {

    protected $primaryKey = 'id_membre';

    public $timestamps = false;

    protected $relations = array('personne', 'abonnement');

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('id_membre', 'id_seance', 'date');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function personne() {
        return $this->belongsTo('App\Models\Personne', 'id_personne');
    }

    public function abonnement() {
        return $this->belongsTo('App\Models\Abonnement', 'id_abonnement');
    }


}