<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distributeur extends Model {

    protected $primaryKey = 'id_distributeur';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('nom', 'telephone', 'adresse', 'cpostal', 'ville', 'pays');
}