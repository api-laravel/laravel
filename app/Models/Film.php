<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model {

    protected $primaryKey = 'id_film';

    public $timestamps = false;

    protected $relations = array('genre', 'distributeur', 'seances');
    
    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('titre', 'resum', 'date_debut_affiche', 'date_fin_affiche', 'duree_minutes', 'annee_production');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function genre() {
        return $this->belongsTo('App\Models\Genre', 'id_genre');
    }

    public function distributeur() {
        return $this->belongsTo('App\Models\Distributeur', 'id_distributeur');
    }


    public function seances() {
        return $this->hasMany('App\Models\Seance', 'id_film');
    }
}