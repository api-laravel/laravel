<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model {

    protected $primaryKey = 'id_genre';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('nom');
}