<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reduction extends Model {

    protected $primaryKey = 'id_reduction';

    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('nom', 'date_debut', 'date_fin', 'pourcentage_reduction');
}