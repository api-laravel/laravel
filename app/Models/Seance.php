<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model {

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $relatationsWithResource = array(
        'salle' => 'salle',
        'personneOuvreur' => 'personne',
        'personneTechnicien' => 'personne',
        'personneMenage' => 'personne'
    );

    /**
     * @return array
     */
    public function getRelatationsWithResource() {
        return $this->relatationsWithResource;
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->relations = array_keys($this->relatationsWithResource);
    }

    // MASS ASSIGNMENT -------------------------------------------------------
    protected $fillable = array('id_film', 'id_salle', 'id_personne_ouvreur', 'id_personne_technicien', 'id_personne_menage', 'debut_seance', 'fin_seance');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    public function salle() {
        return $this->belongsTo('App\Models\Salle', 'id_salle');
    }

    public function personneOuvreur() {
        return $this->belongsTo('App\Models\Personne', 'id_personne_ouvreur');
    }

    public function personneTechnicien() {
        return $this->belongsTo('App\Models\Personne', 'id_personne_technicien');
    }

    public function personneMenage() {
        return $this->belongsTo('App\Models\Personne', 'id_personne_menage');
    }

    public function historiqueMembres() {
        return $this->belongsToMany('App\Models\HistoriqueMembre', 'id_seance');
    }


}