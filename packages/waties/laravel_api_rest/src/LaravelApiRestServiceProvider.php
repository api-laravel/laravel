<?php

namespace Waties\LaravelApiRest;

use Illuminate\Support\ServiceProvider;

class LaravelApiRestServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/laravel_api_rest.php' => config_path('laravel_api_rest.php'),
        ], 'config');

        require_once __DIR__ .'/Http/routes.php';

        $this->loadViewsFrom(__DIR__.'/views', 'laravel_api_rest');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/vendor/laravel_api_rest'),
        ]);

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/laravel_api_rest'),
        ], 'public');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom( __DIR__.'/config/laravel_api_rest.php', 'laravel_api_rest');
    }
}