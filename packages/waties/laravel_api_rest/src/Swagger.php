<?php

namespace Waties\LaravelApiRest;

use App\Http\Requests\Request;
use Doctrine\Common\Inflector\Inflector;
use Illuminate\Support\Facades\Config;

/**
 * Class Swagger
 * @package Waties\LaravelApiRest
 */
class Swagger
{
    /**
     * @var string
     */
    protected $basePath;

    /**
     * @var array
     */
    protected $resources;

    public function __construct()
    {
        $this->basePath = Config::get('laravel_api_rest.default-base-path');
        $this->resources = Config::get('laravel_api_rest.resources');
    }

    public function getColumnsMeta($resource)
    {

        $resourceClassName = $this->getResourceClassName($resource);
        $databaseName = Config::get('database.connections.' . Config::get('database.default') . '.database');
        $model = new $resourceClassName;
        $tableName = $model->getTable();

        $sql = "SELECT COLUMN_NAME name, DATA_TYPE type" .
            " FROM information_schema.COLUMNS" .
            " WHERE TABLE_SCHEMA = '$databaseName' AND TABLE_NAME = '$tableName'";
        $results = \DB::select(\DB::raw($sql));

        $columnsMeta = [];
        foreach ($results as $result) {
            $columnsMeta[$result->name] = $result->type;
        }

        return $columnsMeta;
    }

    /**
     * @param $resource
     * @return string
     */
    protected static function getResourceClassName($resource)
    {
        $modelFiles = \Storage::disk('app')->files('Models');
        foreach ($modelFiles as $file) {
            $parts = explode("/", substr($file, 0, -4));
            $modelClass = 'App\\' . implode('\\', $parts);
            $resourceName = end($parts);

            if (ucfirst($resource) == $resourceName) {
                $resourceClassName = $modelClass;
                return $resourceClassName;
            }
        }
    }

    /**
     * @param $resource
     * @return array
     */
    public function getDefaultResponses($resource)
    {
        $pluralResource = Inflector::pluralize($resource);

        $responses = [
            201 => [
                "description" => "Add a new $resource",
                "schema" => [
                    "\$ref" => "#/definitions/" . ucfirst($resource)
                ]
            ],
            206 => [
                "description" => "Finds $pluralResource paginated",
                "schema" => [
                    "\$ref" => "#/definitions/" . ucfirst($resource) . 'Paginated'
                ]
            ],
            404 => [
                "description" => "The $resource with this id doesn't exist",
                "schema" => [
                    "\$ref" => "#/definitions/Error"
                ]
            ],
            406 => [
                "description" => "Format requested not available, accepted format json and xml"
            ]
        ];
        return $responses;
    }


    /**
     * @return array
     */
    public function getDocs()
    {
        $docs = $this->getCommon();

        foreach ($this->resources as $resource => $data) {
            $actions = $data['actions'];
            $docs = $this->addTag($docs, $resource);
            $docs = $this->addDefinitions($docs, $resource);
            $docs = $this->addDefinitions($docs, $resource, true);
            $docs = $this->addCrud($docs, $resource, $actions);
        }

        return $docs;
    }

    /**
     * @return array
     */
    protected function getCommon()
    {
        $docs = [
            "swagger" => "2.0",
            "basePath" => Config::get('laravel_api_rest.default-base-path'),
            "info" => [
                "description" => "",
                "version" => Config::get('laravel_api_rest.default-api-version'),
                "title" => ""
            ],
            "host" => Config::get('laravel_api_rest.host'),
            "basePath" => Config::get('laravel_api_rest.default-base-path'),
            "tags" => ['Auth'],
            "paths" => [],
            "definitions" => [
                "Error" => [
                    "properties" => [
                        "code" => [
                            "type" => "integer"
                        ],
                        "message" => [
                            "type" => "string"
                        ]
                    ]
                ]
            ]
        ];

        $docs['paths']["/authenticate"] = [
            'post' => [
                "summary" => "Authentificate",
                "produces" => [
                    "application/json",
                    "application/xml"
                ],
                "parameters" => array_merge(
                    [
                        [
                            "name" => "email",
                            "in" => "query",
                            "description" => "Current page",
                            "required" => false,
                            "type" => "string"
                        ],
                        [
                            "name" => "password",
                            "in" => "query",
                            "description" => "Current page",
                            "required" => false,
                            "type" => "string"
                        ]
                    ]
                ),
                "tags" => [
                    "Auth"
                ]
            ]
        ];

        return $docs;
    }

    /**
     * @param array $docs
     * @param string $resource
     * @return array
     */
    protected function addTag($docs, $resource)
    {
        $pluralResource = Inflector::pluralize($resource);
        array_push($docs['tags'], [
            "name" => ucfirst($pluralResource)
        ]);
        return $docs;
    }

    /**
     * @param $swagger
     * @param $resource
     * @param boolean $isStrictModel
     * @return array
     */
    protected function addDefinitions($docs, $resource, $isStrictModel = false)
    {
        $nameResource = ucfirst($resource);
        if ($isStrictModel) {
            $nameResource .= 'StrictModel';
        }

        $docs['definitions'][$nameResource] = [
            "type" => "object",
            "properties" => []
        ];

        $resourceClassName = self::getResourceClassName($resource);
        $model = new $resourceClassName();
        $keyName = $model->getKeyName();

        $columnsMeta = $this->getColumnsMeta($resource);

        foreach ($columnsMeta as $field => $type) {
            if ((!in_array($field, [$keyName]) && !preg_match('/id_\w+|\w+_id/', $field) && !$isStrictModel) || $isStrictModel) {
                $property = [];
                switch ($type) {
                    case "id":
                    case "integer":
                    case "int":
                        $property["type"] = "integer";
                        break;
                    case "float":
                        $property["type"] = "number";
                        break;
                    case "date":
                    case "datetime":
                    case "timestamp":
                        $property["type"] = "string";
                        $property["format"] = "date-time";
                        break;
                    case "boolean":
                        $property["type"] = "boolean";
                        break;
                    default:
                        $property["type"] = "string";
                        break;
                }
                $docs['definitions'][$nameResource]['properties'][$field] = $property;
            }
        }

        unset($docs['definitions'][$nameResource]['properties'][$keyName]);

        if (method_exists($model, 'getRelatationsWithResource')) {
            $relatationsWithResource = $model->getRelatationsWithResource();
        } else {
            $relations = $model->getRelations();
            $relatationsWithResource = array_combine($relations, $relations);
        }

        if (!$isStrictModel) {
            foreach ($relatationsWithResource as $relation => $resourceRelation) {
                $singularizeResourceRelation = Inflector::singularize($resourceRelation);
                $singularizeRelation = Inflector::singularize($relation);
                if ($resourceRelation !== $singularizeResourceRelation) {
                    $model = $this->resources[$singularizeResourceRelation]['model'];
                    $docs['definitions'][$nameResource]['properties'][$relation] = [
                        "type" => "array",
                        "items" => [
                            "\$ref" => "#/definitions/" . ucfirst($singularizeRelation) . 'StrictModel'
                        ]
                    ];
                } else {
                    $model = $this->resources[$singularizeResourceRelation]['model'];
                    $docs['definitions'][$nameResource]['properties'][$relation] = [
                        "\$ref" => "#/definitions/" . ucfirst($resourceRelation) . 'StrictModel'
                    ];
                }
            }
        }

        return $docs;
    }

    /**
     * @param array $docs
     * @param string $resource
     * @param array $actions
     * @return array
     */
    protected function addCrud($docs, $resource, $actions)
    {
        foreach ($actions as $action) {
            $nameMethod = 'get' . ucfirst($action) . 'Action';
            if (method_exists(self::class, $nameMethod)) {

                $pluralResource = Inflector::pluralize($resource);
                $responses = self::getDefaultResponses($resource);

                $parameters = [
                    'id' => [
                        "name" => "id",
                        "in" => "path",
                        "required" => true,
                        "type" => "string"
                    ]
                ];
                switch ($action) {
                    case 'show':
                    case 'create':
                    case 'update':
                    case 'delete':
                        $docs = $this->$nameMethod($docs, $resource, $pluralResource, $responses, $parameters);
                        break;
                    default:
                        $docs = $this->$nameMethod($docs, $resource, $pluralResource, $responses);
                        break;
                }

            }
        }

        return $docs;
    }

    /**
     * @param $docs
     * @param $resource
     * @param $pluralResource
     * @param $responses
     * @return array
     */
    public function getIndexAction($docs, $resource, $pluralResource, $responses)
    {
        $docs['paths']["/$pluralResource"] = [
            'get' => [
                "summary" => "Finds $pluralResource",
                "produces" => [
                    "application/json",
                    "application/xml"
                ],
                "parameters" => array_merge(
                    [
                        [
                            "name" => "token",
                            "in" => "query",
                            "description" => "Token",
                            "required" => false,
                            "type" => "string"
                        ],
                        /*[
                            "name" => "fields",
                            "in" => "query",
                            "description" => "The fields that will be displayed",
                            "required" => false,
                            "type" => "string"
                        ],*/
                        [
                            "name" => "page",
                            "in" => "query",
                            "description" => "Current page",
                            "required" => false,
                            "type" => "number",
                            "format" => "integer"
                        ],
                        [
                            "name" => "per_page",
                            "in" => "query",
                            "description" => "Number of items per page",
                            "required" => false,
                            "type" => "number",
                            "format" => "integer"
                        ],
                        [
                            "name" => "sort",
                            "in" => "query",
                            "description" => "Sorting on attributes separated per semicolon",
                            "required" => false,
                            "type" => "string"
                        ],
                        [
                            "name" => "desc",
                            "in" => "query",
                            "description" => "Default sorting is ascending, contains the names of the attributes that will be sorted in a descending manner",
                            "required" => false,
                            "type" => "string"
                        ]
                    ]
                ),
                "tags" => [
                    ucfirst($pluralResource)
                ],
                "responses" => [
                    200 => [
                        "description" => "Finds $pluralResource",
                        "schema" => [
                            "type" => "array",
                            "items" => [
                                "\$ref" => "#/definitions/" . ucfirst($resource)
                            ]
                        ]
                    ],
                    206 => $responses[206]
                ]
            ]
        ];

        return $docs;
    }

    /**
     * @param $docs
     * @param $resource
     * @param $pluralResource
     * @param $responses
     * @param $parameters
     * @return array
     */
    public function getShowAction($docs, $resource, $pluralResource, $responses, $parameters)
    {
        $docs['paths']["/$resource/{id}"]['get'] = [
            "summary" => "Find $resource by id",
            "description" => "Returns a single " . $resource,
            "parameters" => [
                array_merge($parameters['id'], ["description" => "id of $resource that needs to be fetched"])
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                200 => [
                    "description" => "Find $resource by id",
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource)
                    ]
                ],
                404 => $responses[404]
            ]
        ];

        return $docs;
    }

    /**
     * @param $docs
     * @param $resource
     * @param $pluralResource
     * @param $responses
     * @param $parameters
     * @return mixed
     */
    public function getCreateAction($docs, $resource, $pluralResource, $responses, $parameters)
    {
        $docs['paths']["/$resource"]['post'] = [
            "summary" => "Add a new $resource",
            "parameters" => [
                [
                    "in" => "body",
                    "name" => "body",
                    "description" => "",
                    "required" => true,
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource) . 'StrictModel'
                    ]
                ]
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                201 => $responses[201],
                404 => $responses[404]
            ]
        ];

        return $docs;
    }

    /**
     * @param $docs
     * @param $resource
     * @param $pluralResource
     * @param $responses
     * @param $parameters
     * @return array
     */
    public function getUpdateAction($docs, $resource, $pluralResource, $responses, $parameters)
    {
        $docs['paths']["/$resource/{id}"]['put'] = [
            "summary" => "Update an existing $resource",
            "parameters" => [
                array_merge($parameters['id'], ["description" => "id $resource to update"]),
                [
                    "in" => "body",
                    "name" => "body",
                    "description" => "",
                    "required" => true,
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource) . 'StrictModel'
                    ]
                ]
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                200 => [
                    "description" => "Update an existing $resource",
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource) . 'StrictModel'
                    ]
                ],
                404 => $responses[404]
            ]
        ];

        return $docs;
    }

    /**
     * @param $docs
     * @param $resource
     * @param $pluralResource
     * @param $responses
     * @param $parameters
     * @return array
     */
    public function getDeleteAction($docs, $resource, $pluralResource, $responses, $parameters)
    {
        $docs['paths']["/$resource/{id}"]['delete'] = [
            "summary" => "Delete a $resource",
            "parameters" => [
                array_merge($parameters['id'], ["description" => "id $resource to delete"])
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                204 => [
                    "description" => "Delete a $resource"
                ],
                404 => $responses[404]
            ]
        ];

        return $docs;
    }

}