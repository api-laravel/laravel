<?php

namespace Waties\LaravelApiRest\Http\Controllers;

use Doctrine\Common\Inflector\Inflector;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;
use Illuminate\View\FileViewFinder;
use Waties\LaravelApiRest\Configuration;
//use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class RestController extends BaseController
{
    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var array
     */
    protected $relations;

    public function __construct()
    {
        $router = $this->getRouter();
        $this->config = new Configuration($router);
        $resourceClassName = $this->config->getResourceClassName();
        if (class_exists($resourceClassName)) {
            $model = new $resourceClassName;
            $this->relations = $model->getRelations();
        }
    }


    public function index(Request $request)
    {
        $results = null;
        try {
            $statusCode = 206;

            /* Parameters */
            $perPage = $request->query->getInt('per_page', 25);
            $this->config->setRequest($request);
            $sorting = $this->config->getSorting();

            $resourceClassName = $this->config->getResourceClassName();
            $model = null;

            foreach ($sorting as $key => $order) {
                if (!isset($model)) {
                    $model = $resourceClassName::orderBy($key, $order);
                } else {
                    $model->orderBy($key, $order);
                }
            }

            $results = $model->paginate($perPage)->load($this->relations);
        } catch (\Exception $e) {
            $statusCode = 400;
        } finally {
            return \Response::json($results, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $statusCode = 201;

            $data = $request->request->all();
            $resourceClassName = $this->config->getResourceClassName();
            $result = $resourceClassName::create($data);
        } catch (\Exception $e) {
            $statusCode = 404;
        } finally {
            return \Response::json($result, $statusCode);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resourceClassName = $this->config->getResourceClassName();

        return $resourceClassName::find($id)->load($this->relations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $statusCode = 200;

            $data = $request->request->all();
            $resourceClassName = $this->config->getResourceClassName();
            $model = $resourceClassName::find($id)
                ->update($data);
            $result = $resourceClassName::find($id);
        } catch (\Exception $e) {
            $statusCode = 404;
        } finally {
            return \Response::json($result, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $statusCode = 204;
            $resourceClassName = $this->config->getResourceClassName();

            $resourceClassName::find($id)
                ->delete();
        } catch (\Exception $e) {
            $statusCode = 404;
        } finally {
            return \Response::json(null, $statusCode);
        }
    }
}
