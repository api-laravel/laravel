<?php

namespace Waties\LaravelApiRest\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Waties\LaravelApiRest\Swagger;

class SwaggerController extends BaseController
{
    public function index()
    {
        $swagger = new Swagger();
        $docs = $swagger->getDocs();
        return Response::make($docs, 200, array(
            'Content-Type' => 'application/json'
        ));
    }

}