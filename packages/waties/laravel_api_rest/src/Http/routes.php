<?php

Route::get(Config::get('laravel_api_rest.doc-route'), 'Waties\LaravelApiRest\Http\Controllers\SwaggerController@index');

Route::get(Config::get('laravel_api_rest.api-docs-route'), function() {
    //need the / at the end to avoid CORS errors on Homestead systems.
    $response = response()->view('laravel_api_rest::index', array(
            'secure'         => Request::secure(),
            'urlToDocs'      => \URL::to(Config::get('laravel_api_rest.doc-route')),
            'pathAsset'      => 'vendor/laravel_api_rest'
        )
    );
    
    return $response;
});

Route::group(array('prefix' => Config::get('laravel_api_rest.default-base-path'), 'middleware' => Config::get('laravel_api_rest.middleware')), function()
{
    $resources = Config::get('laravel_api_rest.resources');
    foreach ($resources as $resource => $data) {
        $pluralResource = \Doctrine\Common\Inflector\Inflector::pluralize($resource);
        foreach ($data['actions'] as $action) {
            switch ($action) {
                case 'index':
                    Route::get($pluralResource, '\Waties\LaravelApiRest\Http\Controllers\RestController@index')->middleware();
                    break;
                case 'show':
                    Route::get($resource . '/{id}', '\Waties\LaravelApiRest\Http\Controllers\RestController@show');
                    break;
                case 'create':
                    Route::post($resource, '\Waties\LaravelApiRest\Http\Controllers\RestController@create');
                    break;
                case 'update':
                    Route::put($resource. '/{id}', '\Waties\LaravelApiRest\Http\Controllers\RestController@update');
                    break;
                case 'delete':
                    Route::delete($resource. '/{id}', '\Waties\LaravelApiRest\Http\Controllers\RestController@delete');
                    break;
            }
        }
    }
});