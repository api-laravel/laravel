<?php

namespace Waties\LaravelApiRest;

use Doctrine\Common\Inflector\Inflector;
use Illuminate\Routing\Router;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Http\Request;

/**
 * Resource controller configuration.
 *
 * Class Configuration
 * @package Waties\LaravelApiRest
 */
class Configuration
{

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    protected $resourceName;

    /**
     * @var string
     */
    protected $resourceClassName;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var ParameterBag
     */
    protected $parameters;

    /**
     * Current request.
     *
     * @var Request
     */
    protected $request;

    /**
     * Configuration constructor.
     * @param $router
     */
    public function __construct($router)
    {
        $this->router = $router;
        $this->parameters = new ParameterBag();
        $this->setResourceName();
        $this->setResourceClassName();
        if (class_exists($this->resourceClassName)) {
            $model = new $this->resourceClassName;
            $this->key = $model->getKeyName();
        }
    }

    /**
     * @return ParameterBag
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param ParameterBag $parameters
     * @return $this
     */
    public function setParameters(ParameterBag $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * @return Configuration
     */
    protected function setResourceName()
    {
        if ($this->router->current()) {
            $uriWithoutArgs = $this->router->current()->getCompiled()->getStaticPrefix();
            $parts = explode("/", $uriWithoutArgs);
            $resourceName = end($parts);

            $this->resourceName = ucfirst(Inflector::singularize($resourceName));
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getResourceClassName()
    {
        return $this->resourceClassName;
    }

    /**
     * @return Configuration
     */
    protected function setResourceClassName()
    {
        $modelFiles = \Storage::disk('app')->files('Models');
        foreach ($modelFiles as $file) {
            $parts = explode("/", substr($file, 0, -4));
            $modelClass = 'App\\' . implode('\\', $parts);
            $resourceName = end($parts);

            if ($this->resourceName == $resourceName) {
                $this->resourceClassName = $modelClass;
                return $this;
            }
        }
    }

    /**
     * @return array
     */
    public function getSorting()
    {
        $sort = $this->request->query->get('sort') ? explode(',', $this->request->query->get('sort')) : [$this->key];
        $sortByDesc = $this->request->query->get('desc') ? explode(',', $this->request->query->get('desc')) : [];
        $sorting = [];
        foreach ($sort as $key) {
            if (in_array($key, $sortByDesc)) {
                $sorting[$key] = 'desc';
            } else {
                $sorting[$key] = 'asc';
            }
        }

        return $sorting;
    }

    /**
     * @param $fieldsOfResource
     * @return array
     */
    public function getCriteria($fieldsOfResource)
    {
        $criteria = [];

        foreach ($this->request->query->all() as $name => $value) {
            if (in_array($name, array_keys($fieldsOfResource))) {
                $criteria[$name] = explode(',', $value);
            }
        }

        return $criteria;
    }

    /**
     * @param $fieldsOfResource
     * @return array
     */
    public function getFields($fieldsOfResource)
    {
        $fields = $this->request->get('fields') ? explode(',', $this->request->get('fields')) : [];
        foreach ($fields as $key => $name) {
            if (!in_array($name, array_keys($fieldsOfResource))) {
                unset($fields[$key]);
            }
        }

        return $fields;
    }

    /**
     * @param $parameter
     * @param array $defaults
     * @return array
     */
    public function getRequestParameter($parameter, $defaults = array())
    {
        return array_replace_recursive(
            $defaults,
            $this->request->get($parameter, array())
        );
    }
}